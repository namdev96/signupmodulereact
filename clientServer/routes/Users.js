const express = require('express')
const users = express.Router()
const cors = require('cors')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const User = require('../models/User')
users.use(cors())

process.env.SECRET_KEY = 'secret'

users.post('/register', (req, res) => {
  const today = new Date()
  const userData = {
    Email: req.body.Email,
    Password: req.body.Password,
    Full_Name: req.body.Full_Name,
    Current_Location: req.body.Current_Location,
    created: today
  }

  User.findOne({
    Email: req.body.Email
  })
    .then(user => {
      if (!user) {
        bcrypt.hash(req.body.Password, 10, (err, hash) => {
          userData.Password = hash
          User.create(userData)
            .then(user => {
              res.json({ status: user.Email + 'Registered!' })
            })
            .catch(err => {
              res.send('error: ' + err)
            })
        })
      } else {
        res.json({ error: 'User already exists' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.post('/login', (req, res) => {
  User.findOne({
    Email: req.body.Email
  })
    .then(user => {
      if (user) {
        if (bcrypt.compareSync(req.body.Password, user.Password)) {
          // Passwords match
          const payload = {
            _id: user._id,
            Full_Name: user.Full_Name,
            Current_Location: user.Current_Location,
            Email: user.Email
          }
          let token = jwt.sign(payload, process.env.SECRET_KEY, {
            expiresIn: 1440
          })
          res.send(token)
        } else {
          // Passwords don't match
          res.json({ error: 'User does not exist' })
        }
      } else {
        res.json({ error: 'User does not exist' })
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

users.get('/Interest', (req, res) => {
  var decoded = jwt.verify(req.headers['authorization'], process.env.SECRET_KEY)

  User.findOne({
    _id: decoded._id
  })
    .then(user => {
      if (user) {
        res.json(user)
      } else {
        res.send('User does not exist')
      }
    })
    .catch(err => {
      res.send('error: ' + err)
    })
})

module.exports = users