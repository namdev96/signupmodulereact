const mongoose = require('mongoose')
const Schema = mongoose.Schema

// Create Schema
const UserSchema = new Schema({
  Email: {
    type: String
  },
  Password: {
    type: String
  },
  Full_Name: {
    type: String,
    required: true
  },
  Current_Location: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
})

module.exports = User = mongoose.model('users', UserSchema)